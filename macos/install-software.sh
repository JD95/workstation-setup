#!/bin/bash

echo "Setting up emacs..."
sudo port install emacs
git clone https://gitlab.com/JD95/dot-emacs
rm -r ~/.emacs.d
rm ~/.emacs
mv ./dot-emacs/ ~/.emacs.d/
echo "Done!"

echo "Setting up haskell..."
curl -sSL https://get.haskellstack.org/ | sh
stack install structured-haskell-mode stylish-haskell
echo "Done!"

echo "Setting up rust"
sudo port install rust
echo "Done!"

echo "Setting up python"
curl -sSL https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh | sh
echo "Done!"

exit 0
