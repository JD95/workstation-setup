﻿git clone git@gitlab.com:JD95/dot-emacs.git $PSScriptRoot\dot-emacs
if (Test-Path $env:APPDATA\.emacs.d) {
     rm -r $env:APPDATA\.emacs.d
}
Move-Item -Force .\dot-emacs $env:APPDATA\.emacs.d
cp -Path $env:APPDATA\.emacs.d\default-config.json $env:APPDATA\.emacs.d\config.json