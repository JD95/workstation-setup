#!/bin/bash

echo "Installing xcode command line tools..."
xcode-select --install
echo "Done!"

echo "Downloading macports pkg"
touch macports.pkg
# TODO: NEED TO FIND A WAY TO DOWNLOAD THE PKG FROM GITHUB
sudo chmod 777 macports.pkg
echo "Running macports installer"
sudo installer -verbose -pkg macports.pkg -target /
echo "Done!"

echo "Running software installs..."
# chmod +x install-software.sh
# open -a Terminal.app install-software.sh
