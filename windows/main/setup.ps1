param( [string]$user
     , [string]$32bitPath
     , [string]$64bitPath
     )

function Set-Default-Program-Install-Path(){
    $registryPath = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion"
    Set-ItemProperty -Path $registryPath -Name "ProgramFilesDir" -Value "D:\Program Files"
    Set-ItemProperty -Path $registryPath -Name "ProgramFilesDir (x86)" -Value "D:\Program Files (x86)"
}

function Expand-ZIPFile($file)
{
    Add-Type -assembly "system.io.compression.filesystem"
    [io.compression.zipfile]::ExtractToDirectory($file, $file -replace '.zip$')
}

function Download-And-Install($program, $installerUrl) {
    $installerName = "$PSScriptRoot\" + $program + "-installer.exe"
    echo ("Installing " + $program + "...")
    (New-Object System.Net.WebClient).DownloadFile($installerUrl, $installerName)
    Start-Process -Wait -FilePath $installerName
    Remove-Item $installerName
}


function Install-Notion-6(){
    $installerUrl = "https://notion_content.presonusmusic.com/notion6/6.4.462/Notion-6.4.462-Windows.exe"
    Download-And-Install "Notion" $installerUrl
}

function Install-Unreal(){
    $installerUrl = "https://www.unrealengine.com/download"
    Download-And-Install "Unreal" $installerUrl
}

function Install-GPG(){
    $installerUrl = "https://files.gpg4win.org/gpg4win-3.1.1.exe"
    Download-And-Install "GPG" $installerUrl
}

function Install-Ableton-Live(){
    $installerZipUrl = "http://cdn-downloads.ableton.com/channels/10.0.2/ableton_live_suite_10.0.2_64.zip"
    $installerZipName = "$PSScriptRoot\live.zip"
    echo "Installing Abelton Live..."
    (New-Object System.Net.WebClient).DownloadFile($installerZipUrl, $installerZipName)
    Expand-ZIPFile $installerZipName
    Start-Process -Wait .\live\Setup.msi
    Remove-Item -Force -Recurse .\live
    Remove-Item $installerZipName
}

function Run-If-Folder-Missing($folder, $f) {
    if (! (Test-Path $folder)) {
        &$f
    }
}

function Install-Chocolatey() {
    echo "Installing Chocolately..."
    Set-ExecutionPolicy AllSigned -Scope Process
    iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

function Install-Software()
{
    Run-If-Folder-Missing "C:\ProgramData\chocolatey" Install-Chocolatey

    choco install .\packages.config

    Run-If-Folder-Missing "D:\Program Files (x86)\Gpg4win" Install-GPG
    Run-If-Folder-Missing "D:\Program Files\Notion 6" Install-Notion-6
    Run-If-Folder-Missing "D:\ProgramData\Ableton\Live 10 Suite" Install-Ableton-Live
    Install-Unreal
}

function Install-Zip($downloadUrl, $destination) {
    $zipFile = 'payload.zip'
    (New-Object System.Net.WebClient).DownloadFile($downloadUrl, $zipFile)
    Expand-ZIPFile $zipFile
    Move-Item -Force -Recurse -Path .\payload -Destination $destination
    Remove-Item $zipFile
}

function Install-Zip-If-Missing($folder, $downloadUrl){
    if (! (Test-Path $folder)) {       
        Install-Zip $downloadUrl $folder
    }
}

function Install-Mandelbulb(){
    $bulb3dDownloadUrl = 'https://drive.google.com/uc?authuser=0&id=0B9nExdq4ldhIRkNtUmFXdDN6TkU&export=download'
    $bulberDownloadUrl = 'https://downloads.sourceforge.net/project/mandelbulber/Mandelbulber%20v2/win64%20build/mandelbulber2-win64-2.13-2-standalone.zip?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fmandelbulber%2Ffiles%2FMandelbulber%2520v2%2Fwin64%2520build%2Fmandelbulber2-win64-2.13-2-standalone.zip%2Fdownload&ts=1528082114'
    Install-Zip-If-Missing 'D:\Program Files (x86)\Mandelbulb3D' $bulb3dDownloadUrl
    Install-Zip-If-Mizzing 'D:\Program Files\Mandelbulber' $bulberDownloadUrl
}

function Recover-Vivaldi-Bookmarks($backupDrive){
    echo "Recovering Vivaldi Bookmarks..."
    $bookmarkPath = "$env:APPDATA\..\Local\Vivaldi\User Data\Default\"
    if ((Test-Path "$backupDrive\backups") -and (Test-Path $bookmarkPath)) {
        echo "Recovered!"
        cp "$backupDrive\backups\Bookmarks" ($bookmarkPath + "Bookmarks")
    }
    else {
        echo "Recovery Failed! Backup could not be found!"
    }
}

function Recover-From-Drive($folder, $drive){
    if(!(Test-Path "D:\$folder") -and (Test-Path "$drive\backups\$folder")){
        Robocopy "$drive\backups\$folder" "D:\$folder"     
    } 
}

function Recover-Backups($backupDrive) {
    Recover-From-Drive "current-projects" $backupDrive
    Recover-From-Drive "archives" $backupDrive
    Recover-From-Drive "utilities" $backupDrive
    Recover-Vivaldi-Bookmarks $backupDrive
}

function Configure-Software(){
    MapCapsToCtrl
}

Set-Default-Program-Install-Path
Install-Software
Recover-Backups "E:"