repo_added(){
  echo "$(find /etc/apt -name *.list | xargs cat | grep ^[[:space:]]*deb | grep $1)"
}

sudo apt update

sudo apt install -y \
  git \
  curl \
  gnome-tweak-tool \
  make \
  ubuntu-restricted-extras \
  software-properties-common

if [ -z "$(which nix)" ]; then
  curl https://nixos.org/nix/install | sh
  source ~/.nix-profile/etc/profile.d/nix.sh
  'source ~/.nix-profile/etc/profile.d/nix.sh' >> ~/.bashrc
  nix-channel --add https://nixos.org/channels/nixos-unstable nixos
  'NIX_PATH=~/.nix-defexpr/channels/nixos:$NIX_PATH' >> ~/.bashrc
fi

nix-env -iA \
  nixpkgs.tmux

nix-env -iA cachix -f https://github.com/NixOS/nixpkgs/tarball/889c72032f8595fcd7542c6032c208f6b8033db6
sudo mkdir /etc/nix

nix-env -iA \
  nixpkgs.fira-code \
  nixpkgs.emacs

if [ ! -f ~/.emacs.d/init.org ]; then
  git clone https://gitlab.com/JD95/dot-emacs.git
  rm -rf ~/.emacs.d
  mv dot-emacs ~/.emacs.d 
  cp ~/.emacs.d/default-config.json ~/.emacs.d/config.json
fi

cachix use hie-nix
nix-env -iA hies -f https://github.com/domenkozar/hie-nix/tarball/master

if [ ! -d ~/.emacs.d/lisp/PG ]; then
    git clone https://github.com/ProofGeneral/PG ~/.emacs.d/lisp/PG
    cd ~/.emacs.d/lisp/PG
    make
fi

NIXPKGS_ALLOW_UNFREE=1 nix-env -iA nixos.spotify nixos.vivaldi

if [ -z "$(dpkg -l | grep discord)" ]; then
  wget -O discord.deb "https://discordapp.com/api/download?platform=linux&format=deb"
  sudo dpkg -i ./discord.deb 
  sudo apt --fix-broken install
  rm discord.deb
fi

if [ -z "$(which dropbox)" ]; then
  sudo add-apt-repository "deb [arch=i386,amd64] http://linux.dropbox.com/ubuntu xenial main"
  sudo apt-key adv --keyserver pgp.mit.edu --recv-keys 1C61A2656FB57B7E4DE0F4C1FC918B335044912E
  sudo apt update
  sudo apt-get install dropbox
fi

sudo apt-get install -y anki libcanberra-gtk-module
